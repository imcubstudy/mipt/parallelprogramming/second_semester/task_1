cmake_minimum_required(VERSION 3.17)
project(task_1 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(OpenMP REQUIRED)

add_executable(task_1
    main.cpp
)

target_link_libraries(task_1 PRIVATE OpenMP::OpenMP_CXX)
