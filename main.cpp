// Task 1:
//     - Write a multi-threaded "Hello, world!" program.
//     - Each thread must output its thread id
//     - bonus:
//         - threads output their ids in reverse order
// Comment: bonus task is done!
// Author: Gazizov Yakov

#include <cstdio>
#include <memory>
#include <thread>
#include <omp.h>

int main([[maybe_unused]] int const argc, [[maybe_unused]] char const *argv[])
{
    // set dynamic to false just in case if implementation will allocate different number of threads
    // on every other run
    omp_set_dynamic(static_cast<int>(false));

    // output maximum available number of threads
    auto const maxThreads = omp_get_max_threads();
    std::printf("Max threads %d\n", maxThreads);

    // allocate ready flags on which every thread will wait for its turn
    // NOTE: thread with biggest id can proceed immideately
    auto readyFlags = std::make_unique<bool[]>(maxThreads);
#pragma omp parallel default(none) shared(readyFlags)
    {
        readyFlags[omp_get_thread_num()] = false;
    }
    readyFlags[maxThreads - 1] = true;

    // dispatch threads to do code region in parallel
#pragma omp parallel default(none) shared(readyFlags) firstprivate(maxThreads)
    {
        auto const threadNum = omp_get_thread_num();
        // wait for thread's turn
        while (!readyFlags[threadNum]) {
            std::this_thread::yield();
        }

        // do the thing!
        std::printf("Hello! I'm thread %d\n", threadNum);
        if (threadNum != 0) {
            // allow next thread to proceed if we are not on the last one
            readyFlags[threadNum - 1] = true;
        }
    }

    return 0;
}